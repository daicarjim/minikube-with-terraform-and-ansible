variable "hcloud_token" {}
variable "ssh_key_private" {}
variable "ssh_public_key" {}
variable "server_name" {}
variable "server_size" {}
variable "server_region" {}

# Configure the Hetzner Provider
provider "hcloud" {
    token = var.hcloud_token
}

resource "hcloud_ssh_key" "k8s_admin" {
  name       = "k8s_admin"
  public_key = file(var.ssh_public_key)
}



# Create a web server
resource "hcloud_server" "myblog" {
    image         = "ubuntu-18.04"
    name          = var.server_name
    location      = var.server_region
    server_type   = var.server_size
    ssh_keys      = [hcloud_ssh_key.k8s_admin.id]

    # Install package basics on the droplet using remote-exec to execute ansible playbooks to configure the services
    provisioner "remote-exec" {
        inline = [
          "apt update -yq",
          "apt install git -yq",
          "add-apt-repository ppa:deadsnakes/ppa -yq",
          "apt install python3.8 -yq",
          "sudo apt-get install -y fail2ban",
          "sudo apt install curl -yq",
          "apt-get install  curl apt-transport-https ca-certificates software-properties-common -yq",
          "apt update -yq",

        ]

         connection {
            host        = self.ipv4_address
            type        = "ssh"
            user        = "root"
            private_key = file(var.ssh_key_private)
        }
    }

    # Execute ansible playbooks-install docker using local-exec
    provisioner "local-exec" {
        environment = {
            PUBLIC_IP                 = "self.ipv4_address"
            PRIVATE_IP                = "self.ipv4_address_private"
            ANSIBLE_HOST_KEY_CHECKING = "False"
        }


        working_dir = "docker_ubuntu1804/"
        command     = "ansible-playbook -u root --private-key ${var.ssh_key_private} -i ${self.ipv4_address}, playbook.yml"

    }
    #Install conntrack,kubectl,minikube and helm3
    provisioner "remote-exec" {
        inline = [
          "apt-get install conntrack",
          "sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2 curl",
          "curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -",
          "echo deb https://apt.kubernetes.io/ kubernetes-xenial main | sudo tee -a /etc/apt/sources.list.d/kubernetes.list",
          "sudo apt-get update",
          "sudo apt-get install -y kubectl",
          "curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64",
          "sudo install minikube-linux-amd64 /usr/local/bin/minikube",
          "minikube start --driver=none",
          "curl https://baltocdn.com/helm/signing.asc | sudo apt-key add -",
          "sudo apt-get install apt-transport-https --yes",
          "echo deb https://baltocdn.com/helm/stable/debian/ all main | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list",
          "sudo apt-get update",
          "sudo apt-get install helm",

        ]

         connection {
            host        = self.ipv4_address
            type        = "ssh"
            user        = "root"
            private_key = file(var.ssh_key_private)
        }
    }

}

